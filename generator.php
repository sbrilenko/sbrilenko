<?php
class Generate
{
    /*
     * generate text if value variable is empty
     * by default genearated text is 10 symb
     * in string of numbers and symbols i put space - to simulate the line
     * */
    static function generateRandomString($value,$length = 10)
    {
        $value=trim($value);
        if(empty($value))
        {
            $length=rand($length-2,$length+2);
            $gen = substr(str_shuffle("0123456 789abcdefghi jklmnopq rstuvwx yzABCDE FGHIJKLMNO PQRSTUV WXYZ"), 0, $length);
            return $gen;
        }
        else return $value;
    }

    /*
     * text to image
     * return value is array in json
     * time()+60*20 - if time create is more than 20 minutes - clear data
     * */
    static function createimage($text)
    {
        $im = imagecreate(strlen($text) * 10, 30);
        $bg = imagecolorallocate($im, 255, 255, 255);
        $textcolor = imagecolorallocate($im, 0, 0, 255);
        imagestring($im, 5, 0, 0, $text, $textcolor);
        ob_start(); //Start output buffer.
            imagepng($im); //This will normally output the image, but because of ob_start(), it won't.
            $contents = ob_get_contents(); //Instead, output above is saved to $contents
        ob_end_clean(); //End the output buffer.
        imagedestroy($im);
        return json_encode(array('text'=>$text,'base'=>"data:image/jpeg;base64," . base64_encode($contents),
                                'date'=>time()+60*0.5,/*20 minutes*/
                                'humandate'=>date('d-m-Y H:i:s')));
    }
}

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$msg = $request->msg;
$gentext=Generate::generateRandomString($msg,50);
echo Generate::createimage($gentext);
?>



window.onbeforeunload = function() {
    return "Are you sure? All data will be removed";
};
var records=[];
Appl.controller('Add', function($scope, $http) {
        $scope.addRecords = function () {
            var msg = (typeof $scope.new_record == 'undefibed') ? '' : $scope.new_record;
            /*ajax query, if message is empty-than generate text*/
            $http.post('/generator.php', {msg: msg}).
                success(function (data, status, headers, config) {
                    /*add new object to array*/
                    records.unshift({text: data.text, base: data.base, date: data.date, 'humandate': data.humandate});
                    /*clear*/
                    $scope.new_record = '';
                })

        };
})
Appl.controller('viewManager', function($scope,$interval,$http) {
        /*check every 5 sec*/
        $scope.records = records;
        $interval(function () {
            $http.get('/getTime.php').
                success(function (data, status, headers, config) {
                    var result = document.getElementsByClassName("list");
                    var wrappedResult = angular.element(result);
                    if (records.length > 0) {
                        for (var i = 0; i < records.length; i++) {
                            if (typeof records[i] !== 'undefined' && typeof records[i].date !== 'undefined' && parseInt(records[i].date) <= data) {
                                var allli = wrappedResult.find('li')
                                for (var i = 0; i < allli.length; i++) {
                                    if (angular.element(allli[i]).find('input').val() <= data) {
                                        angular.element(allli[i]).remove()
                                    }
                                }
                                records.splice(i, 1);
                            }
                        }
                    }
                })
        }, 5000);
})